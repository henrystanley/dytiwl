
# Did You Think I Was Lonely?

_Did You Think I Was Lonely?_ (or DTYIWL? for short) is a small work
of conceptual net-art. The piece consists of a website which displays
a list of IP addresses collected from the previous 16 visitors.
Because the list includes the visitor’s own address, every viewer of
the site is presented with a unique set of IPs.

The work engages with concepts such as the responsibility of a website
owner to keep visitor information private, and the vulnerability one experiences
when browsing the web. DYTIWL? derives its name from the tongue-in-cheek
idea that a server will never feel lonely because visitors across the world
are always keeping it company.


## Implementations

_DYTIWL?_ is a very simply program, and has implementations in a couple different
languages/frameworks. Currently there is are versions in the following languages:

- Ruby
- Javascript
- Python
- Io
- Haskell
- C
- Java
- Scheme
- Rust

Each one has slightly different instructions in regards to how
to start the webapp, so check the respective subdirectories for more info.