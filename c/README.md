
# DYTIWL? - C

This is the C implementation of DYTIWL.

It uses Jeremy Williams' excellent [httpserver.h](https://github.com/jeremycw/httpserver.h) library.


## Building

To compile this program you'll need a C compiler, `make`, and `xxd` installed:

    $ make
    
To run:

    $ ./dytiwl
    
This will start DYTIWL listening on `0.0.0.0:8080`.
    


