
#define HTTPSERVER_IMPL
#include "httpserver.h"

#include "index.h"

#define IP_LOG_LEN 16
#define IP_BUF_LEN 88
#define TABLE_BUF_LEN ((IP_LOG_LEN * IP_BUF_LEN) + 15)
#define PAGE_BUF_LEN (TABLE_BUF_LEN + index_html_len)
#define NOT_FOUND_MSG "ERROR 404: Page not found."


/* Utils */

// euclidian modulo
int mod(int a, int b) {
  int m = a % b;
  if (m < 0) m += (b > 0) ? b : -b;
  return m;
}


/* IP Log Ring Buffer */

typedef struct {
  char** ips;
  int capacity;
  int size;
  int end;
} IPLog;

// initialize a new IPLog
IPLog* new_log(int capacity) {
  IPLog* log = malloc(sizeof(IPLog));
  log->ips = malloc(sizeof(char*) * capacity);
  log->capacity = capacity;
  log->size = 0;
  log->end = 0;
  return log;
}

// add an IP to an IPLog
void add_ip(IPLog* log, char* ip) {
  if (log->size < log->capacity) log->size++;
  free(log->ips[log->end]);
  log->ips[log->end] = ip;
  log->end = mod(log->end + 1, log->capacity);
}

// render IPLog to HTML table
void render_table(IPLog* log, char* table_buf) {
  memset(table_buf, 0, TABLE_BUF_LEN);
  strcat(table_buf, "<table>");
  for (int i = 0; i < log->size; i++) {
    int ip_index = mod((log->end - 1) - i, log->capacity);
    strcat(table_buf, log->ips[ip_index]);
  }
  strcat(table_buf, "</table>");
}

// deallocate IPLog
void free_log(IPLog* log) {
  for (int i = 0; i < log->capacity; i++) free(log->ips[i]);
  free(log->ips);
  free(log);
}

// globally accesible IPLog & rendered table buffer
IPLog* g_log;
char* g_table_buf;


/* Formatting */

// format IP into a HTML table row
char* format_ip(char* ip) {
  char* fmt_ip = malloc(IP_BUF_LEN);
  memset(fmt_ip, 0, IP_BUF_LEN);
  strcat(fmt_ip, "<tr>");
  int first = 1;
  char* digits = strtok(ip, ".");
  while (digits) {
    if (first) first = 0;
    else strcat(fmt_ip, "<th>.</th>");
    strcat(fmt_ip, "<th>");
    strcat(fmt_ip, digits);
    strcat(fmt_ip, "</th>");
    digits = strtok(NULL, ".");
  }
  strcat(fmt_ip, "</tr>");
  return fmt_ip;
}

// template variables
char* template_front;
size_t template_front_len;
char* template_back;
size_t template_back_len;

// initialize template variables
void init_template() {
  template_front = index_html;
  template_back = strchr(index_html, '~') + 1;
  template_front_len = ((size_t)template_back - (size_t)index_html) - 1;
  template_back_len = (index_html_len - template_front_len) - 1;
}

// render page template with the provided string inserted
void render_template(char* str, char* page_buf) {
  memset(page_buf, 0, PAGE_BUF_LEN);
  strncat(page_buf, template_front, template_front_len);
  strcat(page_buf, str);
  strncat(page_buf, template_back, template_back_len);
}

// global page buffer
char* g_page_buf;


/* Network Stuff */

// get IP string of current remote host
char* get_client_ip(struct http_request_s* request) {
  struct sockaddr_in addr;
  socklen_t addr_size = sizeof(struct sockaddr_in);
  int res = getpeername(request->socket, (struct sockaddr *)&addr, &addr_size);
  char* ip_buf = malloc(sizeof(char) * 20);
  strcpy(ip_buf, inet_ntoa(addr.sin_addr));
  return ip_buf;
}

// handle incoming HTTP request
void handle_request(struct http_request_s* request) {
  struct http_response_s* response = http_response_init();
  http_response_header(response, "Content-Type", "text/html");
  struct http_string_s path = http_request_target(request);
  if (path.len == 1) { // for root path "/"
    // get & format IP address
    char* ip = get_client_ip(request);
    char* fmt_ip = format_ip(ip);
    free(ip);
    // add IP to log
    add_ip(g_log, fmt_ip);
    // render table & page
    render_table(g_log, g_table_buf);
    render_template(g_table_buf, g_page_buf);
    // set response values
    http_response_status(response, 200);
    http_response_body(response, g_page_buf, strlen(g_page_buf));
  } else { // otherwise 404
    http_response_status(response, 404);
    http_response_body(response, NOT_FOUND_MSG, sizeof(NOT_FOUND_MSG));
  }
  http_respond(request, response);
}


/* Program Entry Point */

int main() {
  // init ip log & page buffer
  g_log = new_log(IP_LOG_LEN);
  g_table_buf = malloc(sizeof(char) * TABLE_BUF_LEN);
  // init template & page buf
  init_template();
  g_page_buf = malloc(sizeof(char) * PAGE_BUF_LEN);
  // init HTTP server
  struct http_server_s* server = http_server_init(8080, handle_request);
  http_server_listen(server);
}
