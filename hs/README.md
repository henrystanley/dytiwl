
# DYTIWL? - Haskell

This is the Haskell implementation of DYTIWL.

It uses the [Snap](http://snapframework.com/) web framework and
the nifty [file-embed](https://hackage.haskell.org/package/file-embed) package.


## Building/Running

To run this program you'll need GHC and `cabal` installed.

You can just invoke `cabal run` to get dependencies and start the server:

    $ cabal run
    
or `cabal build` to compile everything into a nice binary:

    $ cabal build
    
Running either `cabal run` or the compiled binary will start DYTIWL on `0.0.0.0:8000`.
    


