{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.ByteString.Char8 as B
import Control.Concurrent.MVar
import Control.Monad.IO.Class
import Snap.Core
import Snap.Http.Server
import Data.FileEmbed


type BS = B.ByteString

type IPLog = MVar [BS]

tag :: BS -> BS -> BS
tag t x = B.concat ["<", t, ">", x, "</", t, ">"]

formatIP :: BS -> BS
formatIP = B.intercalate (tag "th" ".") . map (tag "th") . B.split '.'

formatLog :: [BS] -> BS
formatLog = tag "table" . B.concat . map (tag "tr") 

formatIndex :: BS -> BS
formatIndex bs = B.concat [iL, bs, iR]
  where (iL:iR:[]) = B.split '~' $ $(embedFile "index.html")

addIP :: IPLog -> Snap ()
addIP logMV = do
  log <- liftIO $ takeMVar logMV
  req <- getRequest
  let ip = formatIP . rqClientAddr $ req
  liftIO $ putMVar logMV $ (formatIP ip :) $ take 15 log
  
index :: IPLog -> Snap ()
index logMV = do
  log <- liftIO $ readMVar logMV
  modifyResponse $ setContentType "text/html"
  writeBS $ formatIndex $ formatLog log

main :: IO ()
main = do
  logMV <- newMVar []
  quickHttpServe $ ifTop (addIP logMV >> index logMV)