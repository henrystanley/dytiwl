
/* Helper methods */

// Make an HTML tag out of a Sequence
Sequence tag := method(t, list("<", t, ">", self, "</", t, ">") join)

// Make IP address string into HTML table row
formatIP := method(ip,
  ip split(".") map(tag("th")) join("." tag("th")) tag("tr")
)


/* IP Log */

AddrLog := Object clone do(
  log := List clone
  add := method(addr,
    if(log size > 15, log pop)
    log prepend(formatIP(addr))
  )
  render := method(log join tag("table"))
)


/* HTML Template */

Template := Object clone do(
  html := File with("index.html") contents
  render := method(html asMutable replaceSeq("{{log}}", AddrLog render))
)


/* Server Stuff

Io doesn't have a built-in HTTP server,
so we roll our own primitive one here.

*/

// A connection to a client
Connection := Object clone do(
  handleSocket := method(s,
    s streamReadNextChunk
    path := s readBuffer betweenSeq("GET ", " HTTP")
    if(path == "/",
      AddrLog add(s host)
      s streamWrite("HTTP/1.0 200 OK\n\n")
      s streamWrite(Template render)
    ,
      s streamWrite("HTTP/1.0 404 Not Found\n\n") 
    )
    s close
  )  
)

// A server which spins up Connections for open client sockets
WebServer := Server clone do(
  setPort(8080)
  handleSocket := method(s, Connection clone asyncSend(handleSocket(s)))
)

// Start the WebServer
WebServer start