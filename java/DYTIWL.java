package dytiwl;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.net.InetSocketAddress;
import java.net.URI;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;


// Main class & program entry point
public class DYTIWL {
  public static void main(String[] args) throws Exception {
    Server server = new Server();
    server.start();
  }
}


// Log of IP Addresses
class IPLog {
  ArrayList<String> log;
  IPLog() {
    log = new ArrayList<String>();
  }

  // add IP address
  public void addIP(String ip) {
    log.add(0, fmtIP(ip));
    if (log.size() > 16) log.remove(16);
  }

  // render log as HTML table
  public String render() {
    String logTable = String.join("", log);
    return "<table>" + logTable + "</table>";
  }

  // format IP string as HTML table row
  private String fmtIP(String ip) {
    String[] nums = ip.split("\\.");
    for (int i = 0; i < nums.length; i++) nums[i] = "<th>" + nums[i] + "</th>";
    return "<tr>" + String.join("<th>.</th>", nums) + "</tr>";
  }
}


// HTML Template
class Template {
  String template;

  // init new template from embedded resource
  Template(String filepath) {
    InputStream stream = this.getClass().getResourceAsStream(filepath);
    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
    template = reader.lines().collect(Collectors.joining("\n"));
  }

  // return template with this substring replaced by the provided value
  public String render(String templateVar, String templateStr) {
    return template.replace(templateVar, templateStr);
  }
}


// DYTIWL Server
class Server {
  IPLog ipLog;
  Template index;
  HttpServer server;
  Server() {
    ipLog = new IPLog();
    try {
      index = new Template("/index.html");
      server = HttpServer.create(new InetSocketAddress(8080), 0);
      server.createContext("/", new Handler());
      server.setExecutor(null);
    } catch (IOException ex) {
      System.out.println("Error when initializing server:");
      System.out.println(ex);
    }
  }

  // start server
  public void start() {
    server.start();
  }

  // Handler for HTTP requests
  class Handler implements HttpHandler {
    @Override
    public void handle(HttpExchange t) throws IOException {
      String response;
      if (t.getRequestURI().compareTo(URI.create("/")) == 0) {
        // for root path "/"
        String ip = t.getRemoteAddress().getAddress().getHostAddress();
        ipLog.addIP(ip);
        response = index.render("{{log}}", ipLog.render());
        t.sendResponseHeaders(200, response.length());
      } else {
        // 404 for any other path
        response = "Not found";
        t.sendResponseHeaders(404, response.length());
      }
      OutputStream os = t.getResponseBody();
      os.write(response.getBytes());
      os.close();
    }
  }
}
