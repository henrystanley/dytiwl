#!/bin/bash

rm -rf build
mkdir build
javac -d ./build DYTIWL.java
echo "Main-Class: dytiwl.DYTIWL" > build/manifest.txt
cd build
jar -cfm dytiwl.jar manifest.txt ../index.html dytiwl/*.class
