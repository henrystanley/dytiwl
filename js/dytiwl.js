const http = require('http');
const fs = require('fs');

const host = 'localhost';
const port = 8000;

const html = fs.readFileSync('index.html', 'utf8');

let ip_log = [];

function format_ip(ip_addr) {
  let nums = ip_addr.split('.').map((x) => `<th>${x}</th>`);
  return `<tr>${nums.join('<th>.</th>')}</tr>`;
}

function add_ip(ip_addr) {
  if (ip_log.length >= 16) ip_log.shift();
  ip_log.push(format_ip(ip_addr));
}

function render_page() {
  let log_table = `<table>${ip_log.join('')}</table>`;
  return html.replace('{{log}}', log_table);
}

const handler = (req, res) => {
  if (req.url == '/') {
    add_ip(req.socket.remoteAddress);
    res.setHeader("Content-Type", "text/html");
    res.writeHead(200);
    res.end(render_page());
  } else res.writeHead(404);
};

const server = http.createServer(handler);
server.listen(port, host, () => console.log("Started DYTIWL."));