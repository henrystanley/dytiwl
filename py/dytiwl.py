
from flask import Flask
from flask import request

def tag(t, v): return f"<{t}>{v}</{t}>"

def format_ip(ip):
  nums = [tag("th", n) for n in ip.split(".")]
  return tag("tr", tag("th", ".").join(nums))
  
log = []

def add_ip(ip):
  global log
  log.insert(0, format_ip(ip))
  log = log[0:16]

template = open('index.html').read()

def render():
  table = tag("table", "".join(log))
  return template.replace("{{log}}", table)

app = Flask(__name__)

@app.route("/")
def index():
  add_ip(request.remote_addr)
  return render()
