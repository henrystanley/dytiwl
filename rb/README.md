
# DYTIWL? - Ruby

This is the Ruby implementation of DYTIWL.

The Ruby version was actually the first implementation,
and the version which currently runs on [dytiwl.istigkeit.xyz](https://dytiwl.istigkeit.xyz).

It uses plain Rack and WEBrick.


## Running

To run this program you'll need Ruby installed, as well as the [Rack](https://rubygems.org/gems/rack) gem:

    $ gem install rack
    
To run:

    $ ruby dytiwl.rb
    
This will start DYTIWL listening on `0.0.0.0:7349`.
    


