require 'rackup'

def tag(t, v) = "<#{t}>#{v}</#{t}>"

def format_ip ip
  nums = ip.split('.').map { |n| tag('th', n) }
  tag('tr', nums.join(tag('th', '.')))
end

$log = []
def add_to_log(item) = $log = $log.unshift(item).take(16)

$html = File.read 'index.html'
def render = $html.gsub('{{log}}', tag('table', $log.join))

app = Proc.new do |env|  
  request = Rack::Request.new(env)
  add_to_log(format_ip request.ip) if request.fullpath == "/"
  ['200', {'Content-Type' => 'text/html'}, [render]]
end

Rackup::Handler::WEBrick.run app, Host: '0.0.0.0', Port: 7349
