use std::collections::VecDeque;
use std::net::{ SocketAddr, IpAddr, IpAddr::{ V4, V6 } };
use std::sync::{ Arc, Mutex };
use askama::Template;
use tokio::net::TcpListener;
use axum::{
    extract::{ ConnectInfo, State },
    response::{ Response, Html, IntoResponse },
    http::StatusCode,
    routing::get,
    Router
};


/// IP Address Log ///

#[derive(Clone)]
struct IpLog {
    addrs: VecDeque<String>
}

impl IpLog {

    // Init new Log
    fn new() -> Self {
        Self { addrs: VecDeque::with_capacity(16) }
    }

    // Format the Log into a string
    fn fmt(&self) -> String {
        let mut table = String::from("<table>");
        for addr in self.addrs.iter() { table.push_str(addr) }
        table.push_str("</table>");
        return table
    }

    // Add an IP Address to the Log and return the new formatted log string
    fn add(&mut self, addr: IpAddr) -> String {
        let mut addr_str = String::from("<tr>");
        let delim = if addr.is_ipv4() { "<th>.</th>" } else { "<th>:</th>" };
        let octets = match addr {
           V4(addr) => Vec::from(addr.octets()),
           V6(addr) => Vec::from(addr.octets())
        };
        for (i, octet) in octets.iter().enumerate() {
            addr_str.push_str("<th>");
            addr_str.push_str(octet.to_string().as_str());
            addr_str.push_str("</th>");
            if i < (octets.len() - 1) { addr_str.push_str(delim) }
        }
        addr_str.push_str("</tr>");
        self.addrs.push_front(addr_str);
        if self.addrs.len() > 16 { self.addrs.truncate(16) };
        self.fmt()
    }

}


/// Template ///

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {
    log_str: String
}

impl IntoResponse for IndexTemplate {
    fn into_response(self) -> Response {
        match self.render() {
            Ok(html) => Html(html).into_response(),
            Err(err) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to render template. Error: {err}")
            ).into_response()
        }
    }
}


/// Handler ///

type AppState = Arc<Mutex<IpLog>>;

async fn handler(
    State(state): State<AppState>,
    connect_info: ConnectInfo<SocketAddr>
) -> impl IntoResponse {
    let log_str = state.lock().unwrap().add(connect_info.ip());
    let template = IndexTemplate { log_str };
    template.into_response()
}


/// Main Server Logic ///

#[tokio::main]
async fn main() {
    // Initialize the shared state
    let state = Arc::new(Mutex::new(IpLog::new()));

    // Build our application with a route
    let app = Router::new().route("/", get(handler)).with_state(state);

    // Run our app
    let listener = TcpListener::bind("127.0.0.1:7349").await.unwrap();
    println!("listening on {}", listener.local_addr().unwrap());
    axum::serve(listener, app.into_make_service_with_connect_info::<SocketAddr>()).await.unwrap();
}

