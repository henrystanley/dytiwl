(import srfi-1 srfi-130 (chicken io) spiffy intarweb uri-common)


;; FORMATTING

(define (tag t x) (string-append "<" t ">" x "</" t ">"))

(define (format-ip ip)
  (let ([delim (tag "th" ".")]
        [nums (map (cut tag "th" <>) (string-split ip "."))])
       (tag "tr" (string-join nums delim))))

(define template
  (call-with-input-file "index.html" (cut read-string #f <>)))

(define (render-template log)
  (letrec ([t-lst (string-split template "~")])
          (string-append (first t-lst) log (second t-lst))))


;; LOG STUFF

(define (take-n n lst)
  (if (> n (length lst)) lst (take lst n)))

(define log '())

(define (add-to-log ip)
  (set! log (take-n 16 (cons (format-ip ip) log))))

(define (render-log) (tag "table" (string-join log)))


;; SPIFFY

(define (render continue)
  (add-to-log (remote-address))
  (send-response status: 'ok body: (render-template (render-log)))
  (continue))

(define (handler continue)
  (let ([path (uri-path (request-uri (current-request)))])
       (if (equal? path '(/ ""))
           (render continue)
           (send-status 404 "Not found"))))

(vhost-map `(("localhost" . ,handler)))

(start-server)
